package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado+numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtrair(Calculadora calculadora){
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado-numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        int resultado = 1;
        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado*numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO dividir(Calculadora calculadora){
        Collections.sort(calculadora.getNumeros(), Collections.reverseOrder());
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado = calculadora.getNumeros().get(0)/calculadora.getNumeros().get(1);;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
}
